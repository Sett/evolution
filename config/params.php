<?php

return [
    'adminEmail' => 'what4me@yandex.ru',
    'dbPrefix'   => 'evo_',
    'dbSuffix'   => '',

    'modifiers' => [
        '\app\modifiers\decorators\LocationName',
        '\app\modifiers\LocationStartTrigger',
        '\app\modifiers\location\Food',
        '\app\modifiers\species\Generation',
        '\app\modifiers\species\Eating',
        '\app\modifiers\species\Identity',
        '\app\modifiers\Aging',
        '\app\modifiers\land\Water',
        '\app\modifiers\land\Ground',
        '\app\modifiers\land\Forest',
        '\app\modifiers\land\Hills',
        '\app\modifiers\Landscape',
        '\app\modifiers\Animal',
        '\app\modifiers\Species',
        '\app\modifiers\species\Strength',
        '\app\modifiers\species\Size',
        '\app\modifiers\decorators\DeadAnimal'
    ]
];
