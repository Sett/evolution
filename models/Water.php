<?php
namespace app\models;

use app\components\LocationObject;
use app\interfaces\ILandscape;
use app\interfaces\ILocationObject;

/**
 * Class Water
 * @package app\models
 * @author Funcraft
 *
 * @property int $id
 * @property int $created
 * @property int $changed
 * @property int $land_id
 * @property int $max_square
 * @property int $current_square
 */
class Water extends LocationObject implements ILocationObject, ILandscape
{
    /**
     * @var string
     */
    protected $name = \app\modifiers\land\Water::ATTRIBUTE;

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName('land_water');
    }

    /**
     * @return string
     */
    public function ruTitle()
    {
        return 'Вода';
    }

    /**
     * @return $this
     */
    public function inc()
    {
        if($this->current_square < $this->max_square){
            $this->current_square = $this->current_square + 1;
            $this->changed = time();
        }

        $this->save();

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentSquare()
    {
        return $this->current_square;
    }

    /**
     * @return int
     */
    public function getSquare()
    {
        return $this->getCurrentSquare();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '<h3>Water</h3>'
        . '<li>Square: ' . $this->current_square
        . ' (created on ' . date('Y-m-d H:i', $this->created)
        . ', changed on ' . date('Y-m-d H:i', $this->changed) . ')</li>';

    }
}