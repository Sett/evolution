<?php
namespace app\models;

use app\components\LocationObject;

/**
 * Class Human
 * @package app\models
 * @author Funcraft
 * @property int $id
 * @property int $land_id
 * @property int $created
 * @property int $changed
 * @property int $amount
 */
class Human extends LocationObject
{
    protected $name = \app\modifiers\Human::ATTRIBUTE;

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName('humans');
    }

    /**
     * @return $this
     */
    public function inc()
    {
            $this->amount  = $this->amount + 1;
            $this->changed = time();

        $this->save();

        return $this;
    }
}