<?php
namespace app\models;

use app\components\LocationEvent;
use app\components\LocationObject;
use app\interfaces\IEvent;
use app\interfaces\IListener;
use app\interfaces\ILocationObject;
use app\interfaces\location\IAttribute;

/**
 * Class Location
 * @package app\models
 *
 * @property int $id
 * @property int $square
 * @property int $created
 * @property int $changed
 * @property int $age
 * @property int $level
 */
class Location extends LocationObject
{
    /**
     * @var ILocationObject[]
     */
    protected $locationAttributes = [];

    /**
     * @var IEvent[]
     */
    protected $locationEvents = [];

    /**
     * @var IListener[][]
     */
    protected $locationEventsListeners = [];

    /**
     * @var array
     */
    protected $catchListeners = [];

    /**
     * @var string[]
     */
    protected $locationContent = [];

    /**
     * @var string
     */
    public $title = '';

    public static function tableName()
    {
        return static::fullTableName('location');
    }

    /**
     * @param ILocationObject $attribute
     * @return $this
     */
    public function setLocationAttribute($attribute)
    {
        $this->locationAttributes[$attribute->getName()] = $attribute;

        return $this;
    }

    /**
     * @param ILocationObject[] $attributes
     * @return $this
     */
    public function setLocationAttributes($attributes)
    {
        $this->locationAttributes = $attributes;

        return $this;
    }

    /**
     * @param string $name
     * @return ILocationObject|null
     */
    public function getLocationAttribute($name)
    {
        $attribute = null;

        if(isset($this->locationAttributes[$name])){
            $attribute = $this->locationAttributes[$name];
        }

        return $attribute;
    }

    /**
     * @param string $attribute
     * @return $this
     */
    public function removeLocationAttribute($attribute)
    {
        if(isset($this->locationAttributes[$attribute])){
            unset($this->locationAttributes[$attribute]);
        }

        return $this;
    }

    /**
     * @return \app\interfaces\ILocationObject[]
     */
    public function getLocationAttributes()
    {
        return $this->locationAttributes;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasLA($name)
    {
        return isset($this->locationAttributes[$name]);
    }

    /**
     * @param string $listenerName
     * @return bool
     */
    public function isCatchable($listenerName)
    {
        return isset($this->catchListeners[$listenerName]) && count($this->catchListeners[$listenerName]);
    }

    /**
     * @param string $listenerName
     * @return IListener[]
     */
    public function getListenerReDispatchers($listenerName)
    {
        return $this->catchListeners[$listenerName];
    }

    /**
     * @param string $listenerName
     * @param IListener $reDispatcher
     * @return $this
     */
    public function catchEventListener($listenerName, $reDispatcher)
    {
        if(!isset($this->catchListeners[$listenerName])){
            $this->catchListeners[$listenerName] = [];
        }

        $this->catchListeners[$listenerName][] = $reDispatcher;

        return $this;
    }

    /**
     * @param string $eventName
     * @param IListener $listener
     * @return $this
     */
    public function registerEventListener($eventName, $listener)
    {
        if($event = $this->getLocationEvent($eventName)){
            $this->setLocationEvent($event->registerListener($listener));
        } else {
            if(!isset($this->locationEventsListeners[$eventName])){
                $this->locationEventsListeners[$eventName] = [];
            }
            $this->locationEventsListeners[$eventName][$listener->getName()] = $listener;
        }

        return $this;
    }

    /**
     * @param string $eventName
     * @param mixed $context
     * @return Location
     */
    public function registerEvent($eventName, $context)
    {
        list($name, $type) = explode('@', $eventName);

        $types = [
            IEvent::TYPE_EVENT    => '\app\components\event\Event',
            IEvent::TYPE_CONTENT  => '\app\components\event\Content',
            IEvent::TYPE_LOCATION => '\app\components\event\Location',
        ];
        $event = \Yii::$container->get($types[$type], [$context]);

        return $this->setLocationEvent($event->setName($eventName));
    }

    /**
     * @param string $eventName
     * @return $this
     */
    public function rmEvent($eventName)
    {
        if(isset($this->locationEvents[$eventName])){
            unset($this->locationEvents[$eventName]);
        }

        return $this;
    }

    /**
     * @param IEvent $event
     * @return $this
     */
    public function setLocationEvent($event)
    {
        if(isset($this->locationEventsListeners[$event->getName()])){
            $listeners = $this->locationEventsListeners[$event->getName()];
            foreach($listeners as $name => $listener){
                $event->registerListener($listener);
                unset($this->locationEventsListeners[$event->getName()][$name]);
            }
            unset($this->locationEventsListeners[$event->getName()]);
        }

        $this->locationEvents[$event->getName()] = $event;

        return $this;
    }

    /**
     * @param string $name
     * @return IEvent|null
     */
    public function getLocationEvent($name)
    {
        return isset($this->locationEvents[$name]) ? $this->locationEvents[$name] : null;
    }

    /**
     * @return IEvent[]|false
     */
    public function getLocationEvents()
    {
        return count($this->locationEvents) ? $this->locationEvents : 0;
    }

    /**
     * @param string $name
     * @param string $content
     * @return $this
     */
    public function setLocationContent($name, $content)
    {
        $this->locationContent[$name] = $content;

        return $this;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getLocationContent($name)
    {
        return isset($this->locationContent[$name]) ? $this->locationContent[$name] : null;
    }

    /**
     * @return string[]
     */
    public function getLocationContents()
    {
        return $this->locationContent;
    }

    /**
     * @return int
     */
    public function getSquare()
    {
        return $this->square;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $format
     * @return bool|int|string
     */
    public function getCreated($format = '')
    {
        return $format ? date($format, $this->created) : $this->created;
    }

    /**
     * @param string $format
     * @return bool|int|string
     */
    public function getChanged($format = '')
    {
        return $format ? date($format, $this->changed) : $this->changed;
    }

    /**
     * @param int $age
     * @return $this
     */
    public function incAge($age = 1)
    {
        $this->age += $age;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnimal()
    {
        return $this->hasOne(Animal::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandWater()
    {
        return $this->hasOne(Water::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandGround()
    {
        return $this->hasOne(Ground::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandForest()
    {
        return $this->hasOne(Forest::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandHills()
    {
        return $this->hasOne(Hills::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHumans()
    {
        return $this->hasOne(Human::className(), ['land_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecies()
    {
        return $this->hasMany(Species::className(), ['land_id' => 'id'])->where(['is_dead' => 0]);
    }

    /**
     * @param int $square
     * @return $this
     */
    public function expandSquare($square)
    {
        $this->square += $square;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $title = $this->getTitle() ? $this->getTitle() : 'Location ' . $this->getId();
        $attributesView = '';
        $attributes = $this->getLocationAttributes();

        foreach($attributes as $attribute){
            if($attribute instanceof IAttribute){
                $attributesView .= $attribute->show();
            }
        }


        return '<h1>' . $title . '</h1>'
        . '<li>Появилась: ' . date('Y-m-d H:i', $this->created) . '</li>'
        . '<li>Площадь: ' . $this->getSquare() . ' m<sup>2</sup></li>'
        . '<li>Возраст (сколько лет известна): ' . $this->age . '</li>'
        . '<li>Уровень: ' . $this->level . '</li>'
        . $attributesView;
    }
}