<?php
namespace app\models;

use app\components\LocationObject;

/**
 * Class Animal
 * @package app\models
 * @author Funcraft
 * @property int $id
 * @property int $land_id
 * @property int $created
 * @property int $changed
 * @property int $alive
 * @property int $dead
 */
class Animal extends LocationObject
{
    const PROBABILITY_DEATH = 30;
    const P_DEATH = 2;

    protected $name = \app\modifiers\Animal::ATTRIBUTE;

    public $isDied = false;

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName('animals');
    }

    /**
     * @return $this
     */
    public function inc()
    {
        $this->addAlive(1);

        if($this->getProbability(static::PROBABILITY_DEATH) == static::P_DEATH){
            $this->addDead(1);
        }

        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return bool
     */
    public function isDied()
    {
        return $this->isDied;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function addAlive($count)
    {
        $this->alive += $count;

        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function addDead($count)
    {
        $this->addAlive(-$count);
        $this->dead += $count;
        $this->setIsDied(true);

        return $this;
    }

    /**
     * @param bool $isDied
     * @return $this
     */
    public function setIsDied($isDied)
    {
        $this->isDied = $isDied;

        return $this;
    }
}