<?php
namespace app\models;

use app\components\LocationObject;

/**
 * Class Species
 * @package app\models
 * @author Funcraft
 *
 * @property int $id
 * @property int $land_id
 * @property int $created
 * @property int $changed
 * @property int $age
 * @property int $sex
 * @property int $kind
 * @property bool $is_predator
 * @property bool $is_diseased
 * @property bool $is_dead
 * @property int $health
 * @property int $size
 * @property int $weight
 * @property int $color
 * @property string $genotype
 */
class Species extends LocationObject
{
    const SEXES = ['male', 'female'];

    protected $name = \app\modifiers\Species::ATTRIBUTE;
    protected $labels = [
        'age'           => 'Возраст',
        'sex'           => 'Пол',
        'is_predator'   => 'Хищник',
        'health'        => 'Здоровье',
        'size'          => 'Размер',
        'weight'        => 'Вес',
        'is_diseased'   => 'Болен'
    ];
    protected $renderFields = [

    ];

    public function init()
    {
        $this->renderFields = [
            'age'           => function($row){return $row['age'] . ' лет';},
            'sex'           => function($row){return $row['sex'] ? 'Ж' : 'М';},
            'is_predator'   => function($row){return $row['is_predator'] ? 'Да' : 'Нет';},
            'health'        => function($row){return $row['health'] . ' коб';},
            'size'          => function($row){return $row['size'] . ' см.';},
            'weight'        => function($row){return number_format($row['weight']/1000, 2) . ' кг.';},
            'is_diseased'   => function($row){return $row['is_diseased'] ? 'Да' : 'Нет';},
        ];

        parent::init();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName('species');
    }

    /**
     * @return array
     */
    public function getRenderFields()
    {
        $fields = $this->renderFields;

        foreach($fields as $name => $attribute){
            if(is_callable($attribute)){
                $fields[$name] = $attribute($this);
            }
        }

        return $fields;
    }

    /**
     * @param string $field
     * @param string $attribute
     * @return $this
     */
    public function addRenderField($field, $attribute)
    {
        $this->renderFields[$field] = $attribute;

        return $this;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getRenderField($name)
    {
        return isset($this->renderFields[$name]) ? $this->renderFields[$name]($this) : '#missed_' . $name;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param string $field
     * @param string $label
     * @return $this
     */
    public function addLabel($field, $label)
    {
        $this->labels[$field] = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getGenotype()
    {
        return $this->genotype;
    }

    /**
     * @param array $genotype
     * @return $this
     */
    public function setGenotype($genotype)
    {
        $this->genotype = json_encode($genotype);

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function upgradeGenotype($key, $value)
    {
        $genotype = $this->decodeGenotype();
        $genotype[$key] = $value;

        $this->setGenotype($genotype)->save();

        return $this;
    }

    /**
     * @param string $genotype
     * @return array
     */
    public function decodeGenotype($genotype = null)
    {
        $genotype = $genotype ?: $this->getGenotype();

        return json_decode($genotype, true);
    }

    /**
     * @return int
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * @param int $kind
     * @return $this
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPredator()
    {
        return $this->is_predator;
    }

    /**
     * @param string $newColor
     * @return $this
     */
    public function changeColor($newColor)
    {
        $this->color = $newColor;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param int $g
     * @return $this
     */
    public function decWeight($g = 10)
    {
        if($this->getWeight() > $g){
            $this->weight -= $g;
            $this->changed = time();
            $this->save();
        }

        return $this;
    }

    /**
     * @param int $g
     * @return $this
     */
    public function incWeight($g = 10)
    {
        $this->weight += $g;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @param int $cm
     * @return $this
     */
    public function incSize($cm = 1)
    {
        $this->size += $cm;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return bool
     */
    public function isDiseased()
    {
        return $this->is_diseased;
    }

    /**
     * @return $this
     */
    public function infect()
    {
        $this->is_diseased = true;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return $this
     */
    public function recover()
    {
        $this->is_diseased = false;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getSex()
    {
        return static::SEXES[$this->sex];
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return $this
     */
    public function incAge($age = 1)
    {
        $this->age += $age;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @param int $points
     * @throws \Exception
     */
    public function decHealth($points = 1)
    {
        if($this->getHealth() <= $points){
            $this->comeDeath();
            $this->changed = time();
            $this->save();
        } else {
            $this->health -= $points;
            $this->changed = time();
            $this->save();
        }
    }

    /**
     * @param int $points
     * @return $this
     */
    public function incHealth($points = 1)
    {
        $this->health += $points;
        $this->changed = time();
        $this->save();

        return $this;
    }

    /**
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->land_id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDead()
    {
        return $this->is_dead;
    }

    /**
     * @return $this
     */
    public function comeDeath()
    {
        $this->is_dead = true;
        $this->save();

        return $this;
    }

    /**
     * @return Species[]
     */
    public function getChildren()
    {
        return $this->relationChildren;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){
            if(!$this->getGenotype()){
                $this->setGenotype([]);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Species[]
     */
    public function getParents()
    {
        return $this->relationParents;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrength()
    {
        return $this->hasOne(Strength::className(), ['species_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getRelationParents()
    {
        return $this->hasMany(Species::className(), ['id' => 'parent'])
                    ->viaTable(Generation::tableName(), ['child' => 'id']);
    }

    /**
     * @return $this
     */
    public function getRelationChildren()
    {
        return $this->hasMany(Species::className(), ['id' => 'child'])
                    ->viaTable(Generation::tableName(), ['parent' => 'id']);
    }
}