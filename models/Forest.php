<?php
namespace app\models;

use app\components\LocationObject;
use app\interfaces\ILandscape;
use app\interfaces\ILocationObject;
use app\modifiers\land\Ground;

/**
 * Class Forest
 * @package app\models
 * @author Funcraft
 *
 * @property int $id
 * @property int $created
 * @property int $changed
 * @property int $land_id
 * @property int $max_square
 * @property int $current_square
 */
class Forest extends LocationObject implements ILocationObject, ILandscape
{
    /**
     * @var string
     */
    protected $name = \app\modifiers\land\Forest::ATTRIBUTE;

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName('land_forest');
    }

    /**
     * @return string
     */
    public function ruTitle()
    {
        return 'Лес';
    }

    /**
     * @param Location $location
     * @return $this
     */
    public function inc($location)
    {
        if($this->current_square < $this->max_square){
            $this->current_square++;
            $this->changed = time();
            $this->save();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '<h3>Forest</h3>'
        . '<li>Square: ' . $this->current_square
        . ' (created on ' . date('Y-m-d H:i', $this->created)
        . ', changed on ' . date('Y-m-d H:i', $this->changed) . ')</li>';
    }

    /**
     * @return int
     */
    public function getSquare()
    {
        return $this->current_square;
    }

    /**
     * @return int
     */
    public function getMaxSquare()
    {
        return $this->max_square;
    }
}