<?php
namespace app\models;

use app\components\LocationObject;

/**
 * Class Strength
 * @package app\models
 * @author Funcraft
 *
 * @property int $id
 * @property int $created
 * @property int $changed
 * @property int $land_id
 * @property int $species_id
 * @property int $strength
 * @property int $type
 */
class Strength extends LocationObject
{
    const PROBABILITY_CHANGE_HEALTH = 20;
    const MARK_INC_HEALTH = 5;
    const MARK_DEC_HEALTH = 5;

    protected $name = \app\modifiers\species\Strength::ATTRIBUTE;

    /**
     * Насколько меняется здоровье при изменении силы
     * @var int
     */
    protected $changeHealth = 0;

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::fullTableName(\app\modifiers\species\Strength::ATTRIBUTE);
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     * @return $this
     */
    public function incStrength($strength)
    {
        $this->strength += $strength;
        if($this->getProbability(self::PROBABILITY_CHANGE_HEALTH) == self::MARK_INC_HEALTH){
            $this->changeHealth += 1;
        }

        return $this;
    }

    /**
     * @param int $strength
     * @return $this
     */
    public function decStrength($strength)
    {
        if($this->strength >= $strength){
            $this->strength -= $strength;
            if($this->getProbability(self::PROBABILITY_CHANGE_HEALTH) == self::MARK_DEC_HEALTH){
                $this->changeHealth -= 1;
            }
        } else {
            $this->strength = 0;
            $this->changeHealth -= 1;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getHealthChanging()
    {
        return $this->changeHealth;
    }
}