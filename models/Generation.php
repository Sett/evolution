<?php
namespace app\models;

use app\components\LocationObject;

/**
 * Class Generation
 * @package app\models
 * @author Funcraft
 *
 * @property int $id
 * @property int $created
 * @property int $parent
 * @property int $child
 * @property int $x
 */
class Generation extends LocationObject
{
    const TYPE_X = 0;
    const TYPE_Y = 1;

    /**
     * @var string
     */
    protected $name = \app\modifiers\species\Generation::ATTRIBUTE;
    protected $parents = [];

    /**
     * @var Species
     */
    protected $newGen = null;

    /**
     * @return string
     */
    public static function tableName()
    {
        return self::fullTableName('species_generation');
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param int|Species $parent
     * @return Generation
     */
    public function setParent($parent)
    {
        $this->parent = is_object($parent) ? $parent->getId() : $parent;

        return $this;
    }

    /**
     * @return Species
     */
    public function getNewGen()
    {
        return $this->newGen;
    }

    /**
     * @param Species $species
     * @return $this
     */
    public function setNewGen($species)
    {
        $this->newGen = $species;

        return $this;
    }

    /**
     * @return int
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param int|Species $child
     * @return $this
     */
    public function setChild($child)
    {
        $this->child = is_object($child) ? $child->getId() : $child;

        return $this;
    }

    /**
     * @param string $format
     * @return int|string
     */
    public function getCreated($format = '')
    {
        return $format ? date($format, $this->created) : $this->created;
    }

    /**
     * @param int $date
     * @return $this
     */
    public function setCreated($date)
    {
        $this->created = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param bool $isX
     * @return $this
     */
    public function setX($isX)
    {
        $this->x = $isX;

        return $this;
    }

    /**
     * @param array $parents
     * @return $this
     */
    public function setParents($parents)
    {
        $this->parents = $parents;

        return $this;
    }

    /**
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @param Species $parentX
     * @param Species $parentY
     * @return $this
     */
    public function born($parentX, $parentY)
    {
        $species = new Species();
        $species->land_id       = $parentX->getLocationId();
        $species->age           = 0;
        $species->created       = time();
        $species->changed       = time();
        $species->sex           = mt_rand(0, 1);
        $species->size          = mt_rand(
                                    abs($parentX->getSize() - $parentY->getSize())+1,
                                    max($parentX->getSize(), $parentY->getSize())+mt_rand(0,5)
                                  );
        $species->color         = 'gray';
        $species->weight        = $species->size * mt_rand(30, 40);
        $species->kind          = mt_rand(0, 1) ? $parentY->getKind() : $parentX->getKind();
        $species->is_predator   = $parentX->isPredator() == $parentY->isPredator()
                                    ? $parentX->isPredator()
                                    : mt_rand(0,1);
        $species->is_diseased   = 0;
        $species->health        = mt_rand(
                                    abs($parentX->getHealth() - $parentY->getHealth())+1,
                                    max($parentX->getHealth(), $parentY->getHealth())+mt_rand(0,5)
                                  );
        $species->setGenotype([$parentX->getId() => 'x', $parentY->getId() => 'y']);
        $species->save();
        $parentX->upgradeGenotype($species->getId(), 'child');
        $parentY->upgradeGenotype($species->getId(), 'child');

        $this->setNewGen($species);

        return $this;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setCreated(time());
            return true;
        } else {
            return false;
        }
    }
}