<?php

use yii\db\Migration;

class m160310_205210_hillsMigration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Hills::tableName() . '}}',[
            'id' => \yii\db\Schema::TYPE_PK,
            'created' => \yii\db\Schema::TYPE_INTEGER,
            'changed' => \yii\db\Schema::TYPE_INTEGER,
            'land_id' => \yii\db\Schema::TYPE_INTEGER,
            'max_square' => \yii\db\Schema::TYPE_INTEGER,
            'current_square' => \yii\db\Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Hills::tableName() . '}}');
    }
}
