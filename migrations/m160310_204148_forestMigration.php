<?php

use yii\db\Migration;

/**
 * Class m160310_204148_forestMigration
 * @author Funcraft
 */
class m160310_204148_forestMigration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Forest::tableName() . '}}',[
            'id' => \yii\db\Schema::TYPE_PK,
            'created' => \yii\db\Schema::TYPE_INTEGER,
            'changed' => \yii\db\Schema::TYPE_INTEGER,
            'land_id' => \yii\db\Schema::TYPE_INTEGER,
            'max_square' => \yii\db\Schema::TYPE_INTEGER,
            'current_square' => \yii\db\Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Forest::tableName() . '}}');
    }
}
