<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m160317_210456_speciesStrength
 * @author Funcraft
 */
class m160317_210456_speciesStrength extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Strength::tableName() . '}}', [
            'id' => Schema::TYPE_PK,
            'location_id' => Schema::TYPE_INTEGER,
            'species_id'  => Schema::TYPE_INTEGER,
            'strength'    => Schema::TYPE_INTEGER,
            'type'        => Schema::TYPE_INTEGER,
            'created'     => Schema::TYPE_INTEGER,
            'changed'     => Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Strength::tableName() . '}}');
    }
}
