<?php

use yii\db\Migration;

/**
 * Class m160309_203842_groundMigration
 * @author Funcraft
 */
class m160309_203842_groundMigration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Ground::tableName() . '}}', [
            'id' => \yii\db\Schema::TYPE_PK,
            'created' => \yii\db\Schema::TYPE_INTEGER,
            'changed' => \yii\db\Schema::TYPE_INTEGER,
            'land_id' => \yii\db\Schema::TYPE_INTEGER,
            'max_square' => \yii\db\Schema::TYPE_INTEGER,
            'current_square' => \yii\db\Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Ground::tableName() . '}}');
    }
}
