<?php

use yii\db\Migration;

/**
 * Class m160310_221414_animalMigration
 * @author Funcraft
 */
class m160310_221414_animalMigration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Animal::tableName() . '}}',[
            'id' => \yii\db\Schema::TYPE_PK,
            'land_id' => \yii\db\Schema::TYPE_INTEGER,
            'created' => \yii\db\Schema::TYPE_INTEGER,
            'changed' => \yii\db\Schema::TYPE_INTEGER,
            'alive'   => \yii\db\cubrid\Schema::TYPE_INTEGER,
            'dead'    => \yii\db\Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Animal::tableName() . '}}');
    }
}
