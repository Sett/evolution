<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m160320_094423_speciesDeadFlag
 * @author Funcraft
 */
class m160320_094423_speciesDeadFlag extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%' . \app\models\Species::tableName() . '}}', 'is_dead', Schema::TYPE_BOOLEAN);
        $this->alterColumn('{{%' . \app\models\Species::tableName() . '}}', 'is_dead', 'DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%' . \app\models\Species::tableName() . '}}', 'is_dead');
    }
}
