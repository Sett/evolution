<?php

use yii\db\Migration;

/**
 * Class m160329_222438_reformGeneration
 * @author Funcraft
 */
class m160329_222438_reformGeneration extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%' . \app\models\Generation::tableName() . '}}');
        $this->addColumn('{{%' . \app\models\Species::tableName() . '}}', 'genotype', \yii\db\Schema::TYPE_TEXT);
    }

    public function safeDown()
    {
        $this->createTable('{{%' . \app\models\Generation::tableName() . '}}',['id' => \yii\db\Schema::TYPE_PK]);
        $this->dropColumn('{{%' . \app\models\Species::tableName() . '}}', 'genotype');
    }
}
