<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m160312_220630_species
 * @author Funcraft
 */
class m160312_220630_species extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Species::tableName() . '}}', [
            'id'            => Schema::TYPE_PK,
            'land_id'       => Schema::TYPE_INTEGER,
            'age'           => Schema::TYPE_INTEGER,
            'sex'           => Schema::TYPE_INTEGER,
            'kind'          => Schema::TYPE_INTEGER,
            'is_predator'   => Schema::TYPE_BOOLEAN,
            'is_diseased'   => Schema::TYPE_BOOLEAN,
            'health'        => Schema::TYPE_INTEGER,
            'size'          => Schema::TYPE_INTEGER,
            'weight'        => Schema::TYPE_INTEGER,
            'color'         => Schema::TYPE_TEXT,
            'created'       => Schema::TYPE_INTEGER,
            'changed'       => Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Species::tableName() . '}}');
    }
}
