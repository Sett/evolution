<?php

use yii\db\Migration;

/**
 * Class m160320_211235_isDeadRename
 * @author Funcraft
 */
class m160320_211235_isDeadRename extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%' . \app\models\Species::tableName() . '}}', 'isDead', 'is_dead');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%' . \app\models\Species::tableName() . '}}', 'is_dead', 'isDead');
    }
}
