<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m160328_204254_generations
 * @author Funcraft
 */
class m160328_204254_generations extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Generation::tableName() . '}}', [
            'id'        => Schema::TYPE_PK,
            'created'   => Schema::TYPE_INTEGER,
            'child'     => Schema::TYPE_INTEGER,
            'parent'    => Schema::TYPE_INTEGER,
            'x'         => Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Generation::tableName() . '}}');
    }
}
