<?php

use yii\db\Migration;

/**
 * Class m160308_230943_initMigration
 */
class m160308_230943_initMigration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%' . \app\models\Location::tableName() . '}}', [
            'id' => \yii\db\pgsql\Schema::TYPE_PK,
            'created' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'changed' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'square'  => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'age'     => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'level'   => \yii\db\pgsql\Schema::TYPE_INTEGER
        ]);

        $this->createTable('{{%' . \app\models\Water::tableName() . '}}',[
            'id' => \yii\db\pgsql\Schema::TYPE_PK,
            'land_id' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'max_square' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'current_square' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'created' => \yii\db\pgsql\Schema::TYPE_INTEGER,
            'changed' => \yii\db\pgsql\Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . \app\models\Location::tableName() . '}}');
        $this->dropTable('{{%'. \app\models\Water::tableName() .'}}');
    }
}
