<?php
namespace app\interfaces;
use app\models\Location;

/**
 * Interface IEvent
 * @package app\interfaces
 * @author Funcraft
 */
interface IEvent extends ILocationObject
{
    const TYPE_EVENT    = 'event';
    const TYPE_CONTENT  = 'content';
    const TYPE_LOCATION = 'location';

    /**
     * @param string $name
     * @param string $type
     * @param string $parent
     * @return string
     */
    public static function constructName($name, $type, $parent = '');

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param Location $location
     * @return $this
     */
    public function register(&$location);

    /**
     * @param IListener $listener
     * @return $this
     */
    public function registerListener($listener);

    /**
     * @return IListener[]
     */
    public function getListeners();

    /**
     * @param mixed|Location $injection
     * @return $this
     */
    public function rise(&$injection);

    /**
     * @return mixed
     */
    public function output();

    /**
     * @param mixed $output
     * @return $this
     */
    public function setOutput($output);

    /**
     * @param mixed $output
     * @return $this
     */
    public function appendOutput($output);

    /**
     * @param mixed $context
     * @return mixed
     */
    public function setContext($context);

    /**
     * @return mixed
     */
    public function getContext();
}