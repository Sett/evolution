<?php
namespace app\interfaces;

/**
 * Interface ILocationObject
 * @package app\interfaces
 * @author Funcraft
 */
interface ILocationObject
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param int $probability
     * @return int
     */
    public function getProbability($probability);
}