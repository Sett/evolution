<?php
namespace app\interfaces\location;

use app\interfaces\ILocationObject;

/**
 * Interface IAttribute
 * @package app\interfaces\location
 * @author Funcraft
 */
interface IAttribute extends ILocationObject
{
    /**
     * @return string
     */
    public function show();
}