<?php
namespace app\interfaces;
use app\models\Location;

/**
 * Interface IListener
 * @package app\interfaces
 * @author Funcraft
 */
interface IListener extends ILocationObject
{
    /**
     * @param IEvent $event
     * @param mixed $injection
     * @return void
     */
    public function dispatch(&$event, &$injection);

    /**
     * @param IEvent $event
     * @param IListener $listener
     * @param Location $location
     * @return void
     */
    public function catchListener(&$event, &$listener, &$location);
}