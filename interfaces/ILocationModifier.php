<?php
namespace app\interfaces;

use app\models\Location;
use yii\db\ActiveQuery;

/**
 * Interface ILocationModifier
 * @package app\interfaces
 * @author Funcraft
 */
interface ILocationModifier extends ILocationObject
{
    /**
     * @param Location $location
     * @return $this
     */
    public function setLocation($location);

    /**
     * @return Location
     */
    public function getLocation();

    /**
     * @return Location
     */
    public function apply();

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    public function appendToLocation($query);
}