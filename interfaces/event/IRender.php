<?php
namespace app\interfaces\event;

/**
 * Interface IRender
 * @package app\interfaces\event
 * @author Funcraft
 */
interface IRender
{
    /**
     * @return self
     */
    public static function getInstance();

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     * @return void
     */
    public static function appendEventOutput($locationId, $eventName, $output);

    /**
     * @param int $locationId
     * @return string
     */
    public static function output($locationId);

    /**
     * @param int $locationId
     * @return string
     */
    public function getOutput($locationId);
}