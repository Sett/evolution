<?php
namespace app\interfaces;

/**
 * Interface ILandscape
 * @package app\interfaces
 * @author Funcraft
 */
interface ILandscape extends ILocationObject
{
    /**
     * @return int
     */
    public function getSquare();

    /**
     * @return string
     */
    public function ruTitle();
}