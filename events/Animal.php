<?php
namespace app\events;

use app\components\event\Event;

/**
 * Class Animal
 * @package app\events
 * @author Funcraft
 */
class Animal extends Event
{
    const NAME = 'animal@event';

    protected $name = self::NAME;
}