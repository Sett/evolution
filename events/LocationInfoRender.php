<?php
namespace app\events;

use app\components\event\Location;

/**
 * Class LocationInfoRender
 * @package app\events
 * @author Funcraft
 */
class LocationInfoRender extends Location
{
    const NAME = 'locationInfoRender@location';

    protected $name = self::NAME;
}