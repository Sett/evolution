<div class="panel panel-default">
    <div class="panel-heading">Детализация по животным (существам)</div>
    <div class="panel-body">
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <?='<th>' . implode('</th><th>', $labels) . '</th>';?>
            </tr>
            </thead>
            <tbody>
                <?=$species?>
            </tbody>
        </table>
    </div>
</div>