<?php
/**
 * @var \app\models\Species $spec
 */
?>
<tr>
    <?php
    $row = '';
    $fields = $spec->getRenderFields();
    foreach($fields as $name => $value){
        $row .= '<td>' . $value . '</td>';
    }
    echo $row;
    ?>
</tr>