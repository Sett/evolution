<?php
use app\components\Nature;
/* @var $this yii\web\View */

$this->title = 'Природа - самостоятельный мир';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Добро пожаловать на сайт Природы</h2>
        <h3>(третья версия Моего Маленького Мира)</h3>

        <p class="lead">Текущая версия: <?=Nature::VERSION?> (дата релиза: <?=Nature::RELEASE_DATE?>)</p>
    </div>

    <div class="body-content">
    </div>
</div>
