<?php
/**
 * @var \app\models\Location $location
 */
?>
<div class="panel panel-info">
    <div class="panel-heading"><?=$location?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8"><?=$contents?></div>
            <div class="col-md-4"><?=$events?></div>
        </div>
    </div>
</div>