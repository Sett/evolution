<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use \app\components\Nature as App;

$this->title = 'Nature (Природа) - Самостоятельный мир - v' . App::VERSION . ' ( дата релиза: ' . App::RELEASE_DATE . ')';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <?=$natureLog?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Changelog</div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.5</div>
                    <div class="panel-body">
                        <li>Изменён принцип поколений. Теперь у существа есть генотип, по которому можно определить родителей.</li>
                        <li class="list-group-item-success">Список модификаторов подгружается из конфига.</li>
                        <li class="list-group-item-success">Модификатор может подцепить сразу несколько relation'ов.</li>
                        <li class="list-group-item-success">Предки и потомки не едят друг друга.</li>
                        <li class="list-group-item-info">RN #7: Оптимизировать обновление данных.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.4</div>
                    <div class="panel-body">
                        <li>Добавлены поколения. Характеристики нового поколения зависят от предыдущих.</li>
                        <li class="list-group-item-info"><span class="label label-success">готово</span> RN #6: Подгружать список модификаторов из конфига.</li>
                        <li class="list-group-item-danger"><span class="label label-success">решено</span> Баг #4: Модификатор не может подцепить сразу 2 relation'а.</li>
                        <li class="list-group-item-danger">Баг #5: При рождении существа как нового поколения, ему не назначается сила.</li>
                        <li class="list-group-item-danger"><span class="label label-success">решено</span> Баг #6: Предки и потомки могут есть друг друга - исключить, ибо проподает смысл размножения.</li>
                        <li class="list-group-item-danger">Баг #7: Невозможно получать/изменять у существа силу/др.связанные свойства через объект самого существа.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.3</div>
                    <div class="panel-body">
                        <li>Значительно переработана охота существ. Теперь хищники могут съесть только более мелких и слабых жертв</li>
                        <li>Хищники теперь заболевают, если съедают больное существо.</li>
                        <li>Хищники теперь от отчаяния могут съесть и труп, что также ведёт к болезни.</li>
                        <li>От болезни существа теперь теряют здоровье и вес.</li>
                        <li>Доработан вывод событий по существам: теперь отображается ход охоты и имена жертв хищников.</li>
                        <li>В детализацию о существах добавлена информация о том, больно ли существо.</li>
                        <li>Если подвести итог этому обновлению, то решена проблема бессмертных первых существ (им всегда достаётся хотя бы немного еды, в результате чего они растут, из-за чего хищники на них не нападают, даже если они сильнее жертв), у хищников появилось больше возможностей прожить дольше, вместе с тем выживать стало и сложнее, т.к. болезнь на данный момент вылечить нельзя.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.2</div>
                    <div class="panel-body">
                        <li>Исправлен баг, когда несколько хищников насыщались одной и той же жертвой.</li>
                        <li>Добавлена идентификация существ.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.1</div>
                    <div class="panel-body">
                        <li>Добавлен модификатор "Еда": теперь в локации появляется пища.</li>
                        <li>Существа теперь питаются - от этого меняются их размеры, вес и здоровье.</li>
                        <li>Если хищнику не хватает еды, он пытается съесть кого-нибудь (первого не хищника, которого встретит).</li>
                        <li>Если существу не хватает еды, он пытается восполнить недостаток за счёт своего жира (по факту - веса).</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.9.0</div>
                    <div class="panel-body">
                        <li class="list-group-item-success">Решена проблема пропадания вида ландшафта при увеличении.</li>
                        <li>Модификатор ландшафта теперь при превышении площади локации, увеличивает её.</li>
                        <li>Полностью переработан вывод "Существ". Теперь можно изменять представление результатов.</li>
                        <li>Модификатор "Сила" аткуализирован и теперь добавляет столбец в вывод "Существ".</li>
                        <li>Существа теперь растут с течением жизни.</li>
                        <li>Добавлены метки для предыдущих версий, чтобы было видно, что сделано.</li>
                        <li class="list-group-item-warning">Обнаружена проблема: если модификатор подписывается на своё же событие, имеющее внешнее представление для пользователя, то вполне вероятна ситуация, когда другие модификаторы регистрируются на это событие позже, что в итоге приводит к тому, что все другие модификаторы оказываются не у дел, т.к. инициализирующий модификатор уже вывел результаты. На данный момент решается перехватом нативного обработчика.</li>
                        <li class="list-group-item-info"><span class="label label-success">готово</span> RN #4: Добавить изменение веса существ по ходу жизни.</li>
                        <li class="list-group-item-info"><span class="label label-success">готово</span> RN #5: Добавить какую-то идентификацию животных, чтобы было понятно, с кем происходят изменения.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.8.2</div>
                    <div class="panel-body">
                        <li>Реализованы рендеры по типам событий.</li>
                        <li class="list-group-item-success">Решена проблема событий, возникающих на этапе запуска других событий.</li>
                        <li>Актуализированы все модификаторы.</li>
                        <li class="list-group-item-info"><span class="label label-success">решено</span> RN #1: Исправить проблему гибели животных и аккумуляции мусорной силы.</li>
                        <li class="list-group-item-info"><span class="label label-success">готово</span> RN #2: Добавить/дополнить модификатор увеличения размера локации.</li>
                        <li class="list-group-item-info"><span class="label label-success">готово</span> RN #3: Добавить рост животных по ходу жизни.</li>
                        <li class="list-group-item-danger"><span class="label label-success">решено</span> Баг #3: Когда в сумме все виды ландшафта становятся больше исходной площади локации, один вид ландашфта перестаёт отображаться.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.8.1</div>
                    <div class="panel-body">
                        <li>Вес существа теперь больше зависит от размера.</li>
                        <li>Изменено определение здоровья существа - теперь это не константа.</li>
                        <li>Добавлен модификатор "Сила существ".</li>
                        <li>Изменён вывод событий.</li>
                        <li>В модификаторы добавлен метод riseEvent для немедленного запуска событий.</li>
                        <li class="list-group-item-danger"><span class="label label-success">решено</span> Баг #1: события, созданные на этапе запуска других событий, не учитываются автоматически - т.е. это ответственность того, кто запускает новое событие.</li>
                        <li class="list-group-item-danger"><span class="label label-success">решено</span> Баг #2: при событии "Гибель существа" теряется информация о том, какие именно существа погибли. В результате этого, копится мусорная информация по мёртвым существам (например, записи о силе).</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.8.0</div>
                    <div class="panel-body">
                        <li>Изменена логика определения веса существ.</li>
                        <li>Изменена модель событий локации: теперь подписываться надёжнее через $location->registerEventListener($eventName, $listener)</li>
                        <li>Добавлены перехватчики событий.</li>
                        <li>Модификатор "Существа" перехватывает событие "Погибло животное".</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.7.3</div>
                    <div class="panel-body">
                        <li>Добавлен модификатор "Существа", который создаёт особи животных.</li>
                        <li>Изменена логика события "Животные" - теперь при создании животного, событие тоже срабатывает.</li>
                        <li>Добавлено уведомление о том, что новое существо появилось.</li>
                        <li>Добавлен вывод детальной информации по существам.</li>
                        <li><span class="label label-success">решено</span> <strong>В следующей версии планируется переработать модель событий в локации.</strong></li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.7.2</div>
                    <div class="panel-body">
                        <li>Добавлен ход времени (с помощью модификатора "Взросление").</li>
                        <li>1 день = 1 год</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.7.1</div>
                    <div class="panel-body">
                        <li>Добавлены типы событий локации.</li>
                        <li>Добавлена возможност делать инъекции в события и обрабатывать их.</li>
                        <li>В рендер локации добавлен учёт типа событий.</li>
                        <li>Заголовок локации русифицирован.</li>
                        <li>Добавлено триггер "Локация найдена".</li>
                        <li>Добавлен модификатор-декоратор "Имя локации", который присваивает локации имя.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.7.0</div>
                    <div class="panel-body">
                        <li>Добавлены события локации.</li>
                        <li>В рендер локации добавлены результаты событий.</li>
                        <li>Добавлено событие "Животные".</li>
                        <li>Добавлен модификатор-декоратор "Мёртвое животное", который выводит сообщение о гибели животного, если в этот ход это произошло.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.6.1</div>
                    <div class="panel-body">
                        <li>Исправлена ошибка рендера животных.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.6.0</div>
                    <div class="panel-body">
                        <li>Изменена логика контента территории - теперь это строки.</li>
                        <li>Изменена логика рендера территории - теперь вывод берётся только из контента территории.</li>
                        <li>В связи с вышеуказанными изменениями актуализированы модификаторы ландшафта и животных</li>
                        <li>В базовый класс модификаторов добавлен метод для рендера модификаторов.</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.5.3</div>
                    <div class="panel-body">
                        <li>Убраны не нужные пункты меню.</li>
                        <li>Добавлено краткое описание "О природе".</li>
                        <li>Добавлена константа текущей версии природы: Nature::VERSION</li>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">v0.5.2</div>
                    <div class="panel-body">
                        <li>Добавлены лес и горы.</li>
                        <li>Для ландшафта добавлен метод hasLand для проверки существования пригодной для жизни земли.</li>
                        <li>Добавлены животные. Сейчас они рождаются и умирают.</li>
                        <li>Добавлен сам changelog.</li>
                        <li>Переоформлен вывод.</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>