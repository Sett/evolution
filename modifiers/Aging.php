<?php
namespace app\modifiers;

use app\components\LocationListener;
use app\events\LocationInfoRender;
use app\interfaces\IListener;
use app\models\Location;

/**
 * Class Aging
 * @package app\modifiers
 * @author Funcraft
 */
class Aging extends LocationListener implements IListener
{
    const ATTRIBUTE = 'aging';
    const RELATION_LOCATION = self::NO_RELATION;
    const ONE_AGE = 86400;

    protected $name = 'aging';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($event = $location->getLocationEvent(LocationInfoRender::NAME)){
            $event->registerListener($this);
        }

        return $location;
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        $pass = time() - $injection->getChanged();

        if($pass >= static::ONE_AGE){
            $injection->incAge();
        }

        $event->setOutput($injection);
    }
}