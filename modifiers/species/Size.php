<?php
namespace app\modifiers\species;

use app\components\LocationListener;
use app\models\Location;
use app\modifiers\Species;

/**
 * Class Size
 * @package app\modifiers\species
 * @author Funcraft
 */
class Size extends LocationListener
{
    const PROBABILITY_CHANGE_STRENGTH = 20;
    const MARK_CHANGE_STRENGTH = 5;
    const RELATION_LOCATION = self::NO_RELATION;
    const ATTRIBUTE = 'species-size';

    const EVENT_INC = 'inc-size-species';
    const EVENT_DEC = 'dec-size-species';

    protected $name = 'species-size';

    public function apply()
    {
        return $this->getLocation()
                    ->registerEventListener(Species::EVENT_AGING, $this)
                    // Ловим родной обработчик, чтобы точно подготовить данные до него
                    ->catchEventListener(Species::ATTRIBUTE, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param \app\interfaces\IListener $listener
     * @param Location $location
     */
    public function catchListener(&$event, &$listener, &$location)
    {
        if($event->getName() == Species::EVENT_AGING) {
            $updateAspects = $event->getContext();

            if (!isset($updateAspects['size'])){
                $this->dispatch($event, $location);
            }
        }

        parent::catchListener($event, $listener, $location);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        $updateAspects = $event->getContext();

        $updateAspects['size'] = 1;
        $event->setContext($updateAspects);
    }
}