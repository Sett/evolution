<?php
namespace app\modifiers\species;

use app\components\LocationListener;
use app\modifiers\Species;

/**
 * Class Identity
 * @package app\modifiers\species
 * @author Funcraft
 */
class Identity extends LocationListener
{
    const RELATION_LOCATION = self::NO_RELATION;
    const ATTRIBUTE = 'species-identity';

    protected $name = self::ATTRIBUTE;

    /**
     * @return $this
     */
    public function apply()
    {
        return $this->getLocation()->registerEventListener(Species::EVENT_RENDER_SPECIES, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param mixed $injection
     */
    public function dispatch(&$event, &$injection)
    {
        $names = ['giant', 'hellsy', 'bobbi', 'doggi', 'vania', 'bear', 'foxxi', 'dummy'];

        /**
         * @var \app\models\Species[] $species
         */
        $species = $event->getContext();
        foreach($species as $index => $spec){
            $spec->addRenderField('identity', function($row) use ($names){
                static $identities = [];

                $name = isset($identities[$row['id']])
                            ? $identities[$row['id']]
                            : $identities[$row['id']] = $names[mt_rand(0, count($names)-1)] . $row['id'];

                return $name;
            });
            $species[$index]->addLabel('identity', 'Имя');
        }
        $event->setContext($species);
    }
}