<?php
namespace app\modifiers\species;

use app\components\LocationEvent;
use app\components\LocationListener;
use app\events\LocationInfoRender;
use app\models\Location;
use app\modifiers\Species;

/**
 * Class Strength
 * @package app\modifiers\species
 * @author Funcraft
 */
class Strength extends LocationListener
{
    const PROBABILITY_CHANGE_STRENGTH = 20;
    const MARK_CHANGE_STRENGTH = 5;
    const RELATION_LOCATION = Species::RELATION_LOCATION . '.strength';
    const ATTRIBUTE = 'species-strength';

    const EVENT_INC = 'inc-strength-species';
    const EVENT_DEC = 'dec-strength-species';

    protected $name = 'species-strength';
    private $attributeName = 'strength';

    /**
     * @param string $event
     * @param string $type
     * @return string
     */
    public static function constructEventName($event, $type = LocationEvent::TYPE_EVENT)
    {
        return LocationEvent::constructName($event, $type, Species::ATTRIBUTE);
    }

    /**
     * @return Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        $location->registerEventListener(LocationInfoRender::NAME, $this)
                 ->registerEventListener(Species::constructEventName(Species::EVENT_BORN), $this)
                 ->registerEventListener(Species::constructEventName(Species::EVENT_DEAD), $this)
                 ->registerEventListener(Generation::EVENT_NEW, $this)
                 ->registerEventListener(self::constructEventName(self::EVENT_INC), $this)
                 ->registerEventListener(Species::EVENT_RENDER_SPECIES, $this)
                 ->catchEventListener(Species::ATTRIBUTE, $this);

        return $location;
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param \app\interfaces\IListener $listener
     * @param Location $location
     */
    public function catchListener(&$event, &$listener, &$location)
    {
        if($event->getName() == Species::EVENT_RENDER_SPECIES){
            $this->dispatch($event, $location);
        }

        parent::catchListener($event, $listener, $location);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        if($event->getName() == self::constructEventName(self::EVENT_INC)){
            /**
             * @var \app\models\Strength $strength
             */
            $strength = $event->getContext();
            $event->appendOutput('<span class="label label-info">У существа изменилась сила: ' . $strength->getStrength() . '</span>');
        } elseif(($event->getName() == Species::constructEventName(Species::EVENT_BORN))
            || ($event->getName() == Generation::EVENT_NEW)) {
            $species = $event->getContext();
            $injection->registerEvent(self::constructEventName(self::EVENT_INC), $this->create($species));
        } elseif($event->getName() == Species::constructEventName(Species::EVENT_DEAD)) {
            // nothing to do
        } elseif($event->getName() == Species::EVENT_RENDER_SPECIES){
            /**
             * @var \app\models\Species[] $species
             */
            $species = $event->getContext();
            foreach($species as $index => $spec){
                $spec->addRenderField('strength', function($row){
                    return $row['strength']['strength'];
                });
                $species[$index]->addLabel('strength', 'Сила');
            }
            $event->setContext($species);
        }else {
            $this->deviant($injection);
            $event->setOutput($injection);
        }
    }

    /**
     * @param Location $location
     * @return Location
     */
    public function deviant(&$location)
    {
        $species = $location->getLocationAttribute(Species::ATTRIBUTE);

        if($species){
            $perc = $this->getProbability(self::PROBABILITY_CHANGE_STRENGTH);
            if($species[$this->attributeName] && ($perc == self::MARK_CHANGE_STRENGTH)){
                $species[$this->attributeName]['strength'] += 1;
                $species[$this->attributeName]->save();
            }
        }

        return $location;
    }

    /**
     * @param \app\models\Species $species
     * @return \app\models\Strength
     */
    public function create($species)
    {
        $strength = new \app\models\Strength();
        $strength->location_id  = $species->getLocationId();
        $strength->species_id   = $species->getId();
        $strength->created      = time();
        $strength->changed      = time();
        $strength->strength     = round($species->getSize() / 10)+1;
        $strength->type         = mt_rand(0,1);
        $strength->save();

        return $strength;
    }
}