<?php
namespace app\modifiers\species;

use app\components\LocationListener;
use app\interfaces\IEvent;
use app\models\Forest;
use app\models\Location;
use app\modifiers\location\Food;
use app\modifiers\Species;

/**
 * Class Eating
 * @package app\modifiers\species
 * @author Funcraft
 */
class Eating extends LocationListener
{
    const ATTRIBUTE = 'eating-species';
    const RELATION_LOCATION = self::NO_RELATION;

    protected $name = 'eating-species';
    protected $applyDead = false;// готов ли хищник сожрать труп

    /**
     * @return $this
     */
    public function apply()
    {
        return $this->getLocation()->registerEventListener(Food::EVENT_FOOD_APPEAR, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        /**
         * @var \app\models\Species[] $species
         */
        $species = $injection->getLocationAttribute(Species::ATTRIBUTE);
        /**
         * @var Food $food
         */
        $food = $event->getContext();

        $foodAmount = $food->getFood();

        foreach($species as $index => $spec){
            if($spec->isDead()){// трупы не питаются
                continue;
            }
            $needFood = round($spec->getSize()/10)+1;

            if($spec->isPredator()){
                $this->hunting($species, $spec, $event, $needFood);
            } else{
                if($foodAmount >= $needFood){// Существо наедается
                    $this->feedUp($foodAmount, $needFood, $food, $event, $spec);
                } elseif($foodAmount){// Еды не хватает, но она ещё есть
                    $this->lessFood($foodAmount, $needFood, $food, $event, $spec);
                } else {// еды больше нет...
                    $this->applyDead = false;// Сбрасываем флаг трупоедства
                    $spec->comeDeath();
                }
            }

            $species[$index] = $spec;
        }

        $event->setContext($food);
    }

    /**
     * @param \app\models\Species[] $species
     * @param \app\models\Species $spec
     * @param IEvent $event
     * @param int $needFood
     */
    protected function hunting(&$species, &$spec, &$event, $needFood)
    {
        $hunted = false;
        foreach($species as $subIndex => $subSpec){
            // сами себя не едим
            if($subSpec->getId() == $spec->getId()){
                continue;
            }

            $genotype = $spec->decodeGenotype();

            if(isset($genotype[$subSpec->getId()])){// не едим родственников
                continue;
            }

            // Нападаем на маленьких и живых
            if(($subSpec->getSize() < $spec->getSize()) && !$subSpec->isDead()){
                $strengthSub = is_numeric($subSpec['strength']) || !$subSpec['strength']// Сила - число или её вообще нет
                            ? $subSpec['strength']
                            : $subSpec['strength']['strength'];
                $strength = is_numeric($spec['strength']) || !$spec['strength']// Сила - число или её вообще нет
                    ? $spec['strength']
                    : $spec['strength']['strength'];
                // Нападаем на слабых
                if($strengthSub < $strength){
                    if($subSpec->getWeight() >= $needFood) {
                        $spec->incHealth(1)// наелись
                             ->incWeight(100);// чуток пополнели после еды
                        $hunted = true;
                    }
                    if($subSpec->isDiseased()){// съели больное существо
                        $spec->infect();
                        $event->appendOutput(
                            $this->render([
                                'amount' => $needFood,
                                'identity' => $spec->getRenderField('identity'),
                                'message' => 'Хищник заболел, сожрав больное существо'
                            ], 'species/eating/danger'
                            )
                        );
                    }
                    $subSpec->comeDeath();// жертва съедена
                    break;
                } else {
                    continue;
                }
            } elseif(!$subSpec->isDead()) {// жертва слишком крупная, ищем другую
                continue;
            } else {// похоже, наткнулись на труп
                if($this->applyDead){// дело совсем плохо, готовы питаться трупами
                    $spec->incWeight(50)// чуток пополнели после еды
                         ->infect();// трупы вредны для здоровья
                    $hunted = true;
                    break;
                } else {// ещё не опустились до трупоедства
                    continue;
                }
            }
        }

        if($this->applyDead && !$hunted){// готовы были сожрать труп, но даже его не нашлось...
            // погибаем от голода
            $spec->comeDeath();
            /*$event->appendOutput($this->render([
                'amount' => $needFood,
                'identity' => $spec->getRenderField('identity'),
                'message' => 'Хищник не нашёл жёртву и погиб от голода'
            ], 'species/eating/danger'));*/
        } elseif(!$hunted && !$this->applyDead){// не наелись и уже готовы жрать трупы
            $this->applyDead = true;
            /*$event->appendOutput(
                $this->render([
                    'amount' => $needFood,
                    'identity' => $spec->getRenderField('identity'),
                    'message' => 'Хищник не нашёл подходящих жертв и теперь готов есть даже трупы'
                ], 'species/eating/danger'
                )
            );*/
            $this->hunting($species, $spec, $event, $needFood);
        }
    }

    /**
     * @param int $foodAmount
     * @param int $needFood
     * @param Food $food
     * @param IEvent $event
     * @param \app\models\Species $spec
     */
    protected function feedUp(&$foodAmount, $needFood, &$food, &$event, &$spec)
    {
        $foodAmount -= $needFood;
        $food->eatFood($needFood);
        $event->appendOutput($this->render([
            'amount'   => $needFood,
            'identity' => $spec->getRenderField('identity')
        ], 'species/eating/success')
        );
        $spec->incHealth(1)
            ->incSize(mt_rand(1, 3))
            ->incWeight(mt_rand(10, $needFood*10));
    }

    /**
     * @param int $foodAmount
     * @param int $needFood
     * @param Food $food
     * @param IEvent $event
     * @param \app\models\Species $spec
     */
    protected function lessFood(&$foodAmount, $needFood, &$food, &$event, &$spec)
    {
        $food->eatFood($foodAmount);// съедаем всё, что осталось
        // Если размер меньше веса И недостаток еды меньше веса
        if(($spec->getSize() <= $spec->getWeight()/100) && (($needFood - $foodAmount) <= $spec->getWeight())){
            // недостаток еды восполняем за счёт веса (жиров)
            $spec->decWeight($needFood - ($needFood - $foodAmount));// худеем

            $event->appendOutput($this->render([
                    'amount' => $needFood,
                    'identity' => $spec->getRenderField('identity')
                ], 'species/eating/warning')
            );
        } else {// Проще говоря, хреново год прошёл
            $spec->decHealth($needFood - ($needFood - $foodAmount));
            $spec->decWeight(10);// худеем

            $event->appendOutput($this->render([
                    'amount' => $needFood,
                    'identity' => $spec->getRenderField('identity')
                ], 'species/eating/warning')
            );
        }

        $foodAmount = 0;
    }
}