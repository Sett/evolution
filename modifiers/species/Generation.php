<?php
namespace app\modifiers\species;

use app\components\LocationListener;
use app\models\Location;
use app\modifiers\Species;

/**
 * Class Generation
 * @package app\modifiers\species
 * @author Funcraft
 */
class Generation extends LocationListener
{
    const ATTRIBUTE = 'species-generation';
    const RELATION_LOCATION = self::NO_RELATION;
    const EVENT_NEW = 'species-new-generation@event';

    /**
     * @return $this
     */
    public function apply()
    {
        return $this->getLocation()
                    ->registerEventListener(Species::EVENT_RENDER_SPECIES, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        if($event->getName() == static::EVENT_NEW){
            $event->appendOutput('<span class="label label-success">Появилось новое поколение</span>');
        } else {// species render

            /**
             * @var \app\models\Species[] $species
             */
            $species = $event->getContext();

            $pairs = ['breed' => []];
            $type = [// Ищем противоположный пол
                \app\models\Species::SEXES[\app\models\Generation::TYPE_X] => \app\models\Generation::TYPE_Y,
                \app\models\Species::SEXES[\app\models\Generation::TYPE_Y] => \app\models\Generation::TYPE_X
            ];

            foreach ($species as $index => $spec) {
                if($spec->isDead()){// Исключаем некрофилию
                    continue;
                }
                if(mt_rand(0,1)){
                    continue;
                }
                $gender = $spec->getSex();

                if (isset($pairs[$type[$gender]])) {
                    $pairs['breed'][] = [$pairs[$type[$gender]], $spec];
                    unset($pairs[$type[$gender]]);
                } else {
                    $pairs[$type[$gender]] = $spec;
                }
            }

            $gen = new \app\models\Generation();

            foreach ($pairs['breed'] as $pair) {
                if(mt_rand(0,1)){
                    continue;
                }
                $gen->born($pair[\app\models\Generation::TYPE_X], $pair[\app\models\Generation::TYPE_Y]);
                $injection->registerEvent(static::EVENT_NEW, $gen->getNewGen())
                          ->registerEventListener(static::EVENT_NEW, $this);
            }
        }
    }
}