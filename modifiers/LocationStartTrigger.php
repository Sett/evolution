<?php
namespace app\modifiers;

use app\components\LocationModifier;
use app\events\LocationInfoRender;

/**
 * Class LocationStartTrigger
 * @package app\modifiers
 * @author Funcraft
 */
class LocationStartTrigger extends LocationModifier
{
    const ATTRIBUTE = 'locationStartTrigger';
    const RELATION_LOCATION = self::NO_RELATION;

    protected $name = 'locationStartTrigger';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();
        $event = new LocationInfoRender($location);
        $event->register($location);

        return $location;
    }
}