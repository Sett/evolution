<?php
namespace app\modifiers\land;

use app\components\LocationModifier;
use app\interfaces\ILocationObject;
use app\models\Forest;

/**
 * Class Hills
 * @package app\modifiers\land
 * @author Funcraft
 */
class Hills extends LocationModifier
{
    const ATTRIBUTE = 'landHills';
    const RELATION_LOCATION = 'landHills';

    const PROBABILITY_NEW = 20;//percentage
    const PROBABILITY_INC = 5;
    const P_NEW = 5;

    /**
     * @var string
     */
    protected $name = 'hills';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($hills = $location->getLocationAttribute(static::ATTRIBUTE)){
            if($this->getProbability(static::PROBABILITY_INC) == static::P_NEW){
                $hills->inc($location);
            }
        } elseif($this->getProbability(static::PROBABILITY_NEW) == static::P_NEW) {
            $location->setLocationAttribute($this->create());
        }

        return $location;
    }

    /**
     * @return ILocationObject
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Hills::className());
        $hills = new $class();
        $hills->land_id = $this->getLocation()->getId();
        $hills->max_square = $this->getLocation()->getSquare();

        /**
         * @var Forest $forest
         */
        $forest = $this->getLocation()->getLocationAttribute(\app\modifiers\land\Forest::ATTRIBUTE);

        if($forest){
            $hills->max_square = $hills->max_square - $forest->getSquare();
        }

        $hills->max_square = $hills->max_square > 0 ? $hills->max_square : 1;

        $hills->current_square = mt_rand(1, $hills->max_square);
        $hills->created = time();
        $hills->changed = time();
        $hills->save();

        return $hills;
    }
}