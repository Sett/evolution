<?php
namespace app\modifiers\land;

use app\components\LocationModifier;
use app\interfaces\ILocationObject;

/**
 * Class Water
 * @package app\modifiers\land
 * @author Funcraft
 */
class Water extends LocationModifier
{
    const ATTRIBUTE = 'landWater';
    const RELATION_LOCATION = 'landWater';

    const PROBABILITY = 2;//percentage
    const P_NEW = 5;

    /**
     * @var string
     */
    protected $name = 'water';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($water = $location->getLocationAttribute(static::ATTRIBUTE)){
            if($this->getProbability(static::PROBABILITY) == static::P_NEW){
                $water->inc();
            }
        } else {
            $location->setLocationAttribute($this->create());
        }

        return $location;
    }

    /**
     * @return ILocationObject
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Water::className());
        $water = new $class();
        $water->land_id = $this->getLocation()->getId();
        $water->max_square = $this->getLocation()->getSquare();
        $water->current_square = mt_rand(1, $water->max_square);
        $water->created = time();
        $water->changed = time();
        $water->save();

        return $water;
    }
}