<?php
namespace app\modifiers\land;

use app\components\LocationModifier;
use app\interfaces\ILocationObject;
use app\models\Ground;

/**
 * Class Forest
 * @package app\modifiers\land
 * @author Funcraft
 */
class Forest extends LocationModifier
{
    const ATTRIBUTE = 'landForest';
    const RELATION_LOCATION = 'landForest';

    const PROBABILITY_NEW = 20;//percentage
    const PROBABILITY_INC = 5;
    const P_NEW = 5;

    /**
     * @var string
     */
    protected $name = 'forest';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($forest = $location->getLocationAttribute(static::ATTRIBUTE)){
            if($this->getProbability(static::PROBABILITY_INC) == static::P_NEW){
                $forest->inc($location);
            }
        } elseif($this->getProbability(static::PROBABILITY_NEW) == static::P_NEW) {
            $location->setLocationAttribute($this->create());
        }

        return $location;
    }

    /**
     * @return ILocationObject
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Forest::className());
        $forest = new $class();
        $forest->land_id = $this->getLocation()->getId();
        $forest->max_square = $this->getLocation()->getSquare();

        /**
         * @var Ground $ground
         */
        $ground = $this->getLocation()->getLocationAttribute(\app\modifiers\land\Ground::ATTRIBUTE);

        if($ground){
            $forest->max_square = $forest->max_square - $ground->getSquare();
        }

        $forest->current_square = mt_rand(1, $forest->max_square);
        $forest->created = time();
        $forest->changed = time();
        $forest->save();

        return $forest;
    }
}