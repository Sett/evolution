<?php
namespace app\modifiers\land;

use app\components\LocationModifier;
use app\interfaces\ILocationObject;
use app\models\Water;

/**
 * Class Ground
 * @package app\modifiers\land
 * @author Funcraft
 */
class Ground extends LocationModifier
{
    const ATTRIBUTE = 'landGround';
    const RELATION_LOCATION = 'landGround';

    const PROBABILITY_NEW = 20;//percentage
    const PROBABILITY_INC = 5;
    const P_NEW = 5;

    /**
     * @var string
     */
    protected $name = 'ground';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($ground = $location->getLocationAttribute(static::ATTRIBUTE)){
            if($this->getProbability(static::PROBABILITY_INC) == static::P_NEW){
                $ground->inc($location);
            }
        } elseif($this->getProbability(static::PROBABILITY_NEW) == static::P_NEW) {
            $location->setLocationAttribute($this->create());
        }

        return $location;
    }

    /**
     * @return ILocationObject
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Ground::className());
        $ground = new $class();
        $ground->land_id = $this->getLocation()->getId();
        $ground->max_square = $this->getLocation()->getSquare();

        /**
         * @var Water $water
         */
        $water = $this->getLocation()->getLocationAttribute(\app\modifiers\land\Water::ATTRIBUTE);

        if($water && ($water->getCurrentSquare() < $ground->max_square)){
            $ground->max_square = $ground->max_square - $water->getCurrentSquare();
        }

        $ground->current_square = mt_rand(1, $ground->max_square);
        $ground->created = time();
        $ground->changed = time();
        $ground->save();

        return $ground;
    }
}