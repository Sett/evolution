<?php
namespace app\modifiers;

use app\components\LocationEvent as E;
use app\components\LocationListener;
use app\events\Animal;
use app\interfaces\IListener;
use app\models\Location;
use app\modifiers\decorators\DeadAnimal;
use yii\db\ActiveQuery;

/**
 * Class Species
 * @package app\modifiers
 * @author Funcraft
 */
class Species extends LocationListener implements IListener
{
    const ATTRIBUTE = 'species';
    const RELATION_LOCATION = 'species';

    const EVENT_DEAD = 'dead-species';
    const EVENT_BORN = 'born-species';
    const EVENT_AGING = 'aging-species@event';
    const EVENT_RENDER_SPECIES = 'render-species@content';

    protected $name = self::ATTRIBUTE;

    /**
     * @param string $event
     * @return string
     */
    public static function constructEventName($event = '')
    {
        return E::constructName($event, E::TYPE_EVENT, \app\modifiers\Animal::ATTRIBUTE);
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function appendToLocation($query)
    {
        return static::RELATION_LOCATION != self::NO_RELATION
            ? $query->with([
                static::RELATION_LOCATION => function($query){
                    /**
                     * @var ActiveQuery $query
                     */
                    $query->andWhere([\app\models\Species::tableName() . '.is_dead' => false]);
                }
            ])
            : $query;
    }

    /**
     * @return \app\models\Location
     * @throws \yii\base\InvalidConfigException
     */
    public function apply()
    {
        $location = $this->getLocation();
        $species  = $location->getLocationAttribute(self::ATTRIBUTE);
        // Сначала выясняем есть ли уже созданные существа
        if($species){
            $location->registerEvent(self::EVENT_AGING, ['age' => 1, 'health' => -1])
                     ->registerEventListener(self::EVENT_AGING, $this);

            $location->registerEvent(self::EVENT_RENDER_SPECIES, $species)
                     ->registerEventListener(self::EVENT_RENDER_SPECIES, $this);
        }

        return $location->registerEventListener(Animal::NAME, $this)
                        ->catchEventListener(DeadAnimal::ATTRIBUTE, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param IListener $listener
     * @param \app\models\Location $location
     */
    public function catchListener(&$event, &$listener, &$location)
    {
        $name = $event->getName();

        if($name == Animal::NAME){
            /**
             * @var \app\models\Animal $animal
             */
            $animal = $event->getContext();
            if($animal->isDied()){
                $animal->setIsDied(false);
            }
            $event->setContext($animal);
        }

        parent::catchListener($event, $listener, $location);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        if($event->getName() == self::EVENT_RENDER_SPECIES){
            $content = '';
            /**
             * @var \app\models\Species[] $species
             */
            $species = $event->getContext();
            $labels = [];
            foreach($species as $spec){
                if($spec->isDiseased()){
                    $spec->decHealth(2 + round(10/$spec->getHealth()));// от болезни существо теряет здоровье
                    $spec->decWeight(round($spec->getWeight()/100) * 5);// от болезни существо теряет % веса
                }
                $content .= $this->render(['spec' => $spec], 'species/species');
                if(empty($labels)){
                    $labels = $spec->getLabels();
                }
            }
            $event->setOutput(
                $this->render(['species' => $content, 'labels' => $labels], 'species/species_total')
            );
        } elseif($event->getName() == self::EVENT_AGING){
            $location = $injection;
            $updateAspects = $event->getContext();
            // Стареем (:
            /**
             * @var \app\models\Species $speciesModel
             */
            $speciesModel = \Yii::$container->get(\app\models\Species::className());

            $speciesModel->updateAllCounters(
                $updateAspects,
                ['land_id' => $location->getId(), 'is_dead' => false]
            );

            $deadSpeciesIds = $speciesModel->find()->where(['<=', 'health', 0])->column();

            $dead = $speciesModel->updateAll(
                ['is_dead' => true],
                ['<=','health', 0]
            );
            if($dead){
                $location->registerEvent(
                    static::constructEventName(self::EVENT_DEAD),
                    $deadSpeciesIds
                );
            }
        }else {// new Animal event
            /**
             * @var \app\models\Animal $info
             */
            $info = $event->getContext();

            if(!$info->isDied()){
                $injection->registerEvent(self::constructEventName(self::EVENT_BORN), $this->create());
                $event->appendOutput('<span class="label label-success">Появилось существо</span>');
            } else {
                $info->addAlive(1)->setIsDied(false)->save();
            }
        }
    }

    /**
     * @return \app\models\Species
     */
    public function create()
    {
        $species = new \app\models\Species();
        $species->land_id       = $this->getLocation()->getId();
        $species->age           = 0;
        $species->created       = time();
        $species->changed       = time();
        $species->sex           = mt_rand(0, 1);
        $species->size          = mt_rand(10, 200);
        $species->color         = 'gray';
        $species->weight        = $species->size * mt_rand(30, 40);
        $species->kind          = mt_rand(0, 5);
        $species->is_predator   = mt_rand(0, 1);
        $species->is_diseased   = 0;
        $species->health        = mt_rand(2, 18);
        $species->save();

        return $species;
    }
}