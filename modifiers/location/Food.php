<?php
namespace app\modifiers\location;

use app\components\LocationListener;
use app\events\LocationInfoRender;
use app\interfaces\location\IAttribute;
use app\models\Location;

class Food extends LocationListener implements IAttribute
{
    const ATTRIBUTE = 'food';
    const RELATION_LOCATION = self::NO_RELATION;

    const EVENT_FOOD_APPEAR = 'food-species@event';

    protected $name = 'food';
    protected $food = 0;

    public function apply()
    {
        return $this->getLocation()->registerEventListener(LocationInfoRender::NAME, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        if($event->getName() == self::EVENT_FOOD_APPEAR){
            $event->appendOutput('<span class="label label-success">Появилась еда</span>');
        } else {
            $injection->setLocationAttribute($this->generateFood($injection));
        }
    }

    /**
     * @return string
     */
    public function show()
    {
        return '<span class="label label-info">Пища: ' . $this->getFood() . '</span>';
    }

    /**
     * @param Location $location
     * @return $this
     */
    protected function generateFood($location)
    {
        $this->food = mt_rand(1, round($location->getSquare()/2));

        $location->registerEvent(self::EVENT_FOOD_APPEAR, $this)
                 ->registerEventListener(self::EVENT_FOOD_APPEAR, $this);

        return $this;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function eatFood($amount)
    {
        $this->food = $this->food >= $amount ? $this->food - $amount : 0;

        return $this;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }
}