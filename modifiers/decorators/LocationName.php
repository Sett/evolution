<?php
namespace app\modifiers\decorators;

use app\components\LocationListener;
use app\events\LocationInfoRender;
use app\interfaces\IListener;
use app\models\Location;

/**
 * Class LocationName
 * @package app\modifiers\decorators
 * @author Funcraft
 */
class LocationName extends LocationListener implements IListener
{
    const ATTRIBUTE = 'locationName';
    const RELATION_LOCATION = self::NO_RELATION;

    protected $name = 'locationName';

    /**
     * @return Location
     */
    public function apply()
    {
        return $this->getLocation()->registerEventListener(LocationInfoRender::NAME, $this);
    }

    /**
     * @param \app\interfaces\IEvent $event
     * @param Location $injection
     */
    public function dispatch(&$event, &$injection)
    {
        $event->setOutput($injection->setTitle('Колыбель Мира'));
    }
}