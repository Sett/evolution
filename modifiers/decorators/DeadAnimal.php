<?php
namespace app\modifiers\decorators;

use app\components\LocationListener;
use app\events\Animal;
use app\interfaces\IEvent;
use app\interfaces\IListener;
use app\modifiers\Species;

/**
 * Class DeadAnimal
 * @package app\modifiers\decorators
 * @author Funcraft
 */
class DeadAnimal extends LocationListener implements IListener
{
    const ATTRIBUTE = 'deadAnimal';
    const RELATION_LOCATION = self::NO_RELATION;

    protected $name = self::ATTRIBUTE;

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        return $this->getLocation()
                    ->registerEventListener(Animal::NAME, $this)
                    ->registerEventListener(Species::constructEventName(Species::EVENT_DEAD), $this);
    }

    /**
     * @param IEvent $event
     * @param mixed $injection
     */
    public function dispatch(&$event, &$injection = null)
    {
        if($event->getName() == Animal::NAME){
            /**
             * @var \app\models\Animal $animal
             */
            $animal = $event->getContext();

            if($animal && $animal->isDied()){
                $event->appendOutput('<span class="label label-danger">Погибло животное</span>');
            }
        } else {// Species::EVENT_DEAD
            $event->appendOutput('<span class="label label-danger">Погибло существо</span>');
        }
    }
}