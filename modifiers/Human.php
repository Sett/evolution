<?php
namespace app\modifiers;

use app\components\LocationModifier;
use app\interfaces\ILocationObject;

/**
 * Class Human
 * @package app\modifiers
 * @author Funcraft
 */
class Human extends LocationModifier
{
    const ATTRIBUTE = 'humans';
    const RELATION_LOCATION = 'humans';

    const PROBABILITY = 2;//percentage
    const P_NEW = 7;

    const START_AMOUNT = 2;

    protected $name = 'humans';

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();

        if($modify = $location->getLocationAttribute(static::ATTRIBUTE)){
            if($this->getProbability(static::PROBABILITY) == static::P_NEW){
                $modify->inc();
            }
        } else {
            if($this->getProbability(static::PROBABILITY) == static::P_NEW){
                $location->setLocationAttribute($this->create());
            }
        }

        return $location;
    }

    /**
     * @return ILocationObject
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Human::className());
        $modify = new $class();
        $modify->land_id = $this->getLocation()->getId();
        $modify->amount  = static::START_AMOUNT;
        $modify->created = time();
        $modify->changed = time();
        $modify->save();

        return $modify;
    }
}