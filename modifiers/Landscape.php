<?php
namespace app\modifiers;

use app\components\LocationModifier;
use app\interfaces\ILandscape;
use app\modifiers\land\Forest;
use app\modifiers\land\Ground;
use app\modifiers\land\Hills;
use app\modifiers\land\Water;
use yii\web\View;

/**
 * Class Landscape
 * @package app\modifiers
 * @author Funcraft
 */
class Landscape extends LocationModifier
{
    const ATTRIBUTE = 'landscape';
    const RELATION_LOCATION = self::NO_RELATION;

    protected $name = 'landscape';
    protected $landscapeTypes = [
        Water::ATTRIBUTE  => 'progress-bar-striped active',
        Ground::ATTRIBUTE => 'progress-bar-warning',
        Forest::ATTRIBUTE => 'progress-bar-success',
        Hills::ATTRIBUTE  => 'progress-bar-warning progress-bar-striped'
    ];

    /**
     * @var ILandscape[]
     */
    protected $landscape = [];
    protected $squares = [];

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();
        $types = $this->getTypes();
        $totalSquare = 0;

        foreach($types as $type => $view){
            if($typeObject = $location->getLocationAttribute($type)){
                /**
                 * @var ILandscape $typeObject
                 */
                $location->removeLocationAttribute($type);
                $this->landscape[$type] = $typeObject;
                $this->squares[$type]   = $typeObject->getSquare();
                $totalSquare += $this->squares[$type];
            }
        }

        if($totalSquare > $location->getSquare()){
            $location->expandSquare($totalSquare - $location->getSquare());
        }

        $location->setLocationContent($this->getName(), $this->content());

        return $location;
    }

    /**
     * @param string $type
     * @return int
     */
    public function getSquare($type)
    {
        return isset($this->squares[$type]) ? $this->squares[$type] : 0;
    }

    /**
     * @return bool
     */
    public function hasLand()
    {
        $landing = [Ground::ATTRIBUTE, Forest::ATTRIBUTE, Hills::ATTRIBUTE];

        foreach($landing as $land){
            if(isset($this->squares[$land])){
                return true;
            }
        }
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->landscapeTypes;
    }

    /**
     * @return string
     */
    public function content()
    {
        $maxPerc = $this->getLocation()->getSquare();

        $types = '<div class="progress">';

        foreach($this->landscape as $type => $item){
            $curPerc = round($item->getSquare()/($maxPerc/100));
            $types .= '
                  <div class="progress-bar '. $this->landscapeTypes[$type] .'" role="progressbar" aria-valuenow="'
                    . $curPerc .'" aria-valuemin="0" aria-valuemax="100" style="width: '
                    . $curPerc . '%" title="' . $item->ruTitle() . ': ' . $item->getSquare() . ' м2">
                    <span class="sr-only">&nbsp;</span>
                  </div>';
        }

        $types .= '</div>';

        return '
        <div class="panel panel-default">
          <div class="panel-heading">Ландшафт</div>
          <div class="panel-body">
            ' . $types . '
          </div>
        </div>';
    }
}