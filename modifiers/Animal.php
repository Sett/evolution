<?php
namespace app\modifiers;

use app\components\LocationModifier;

/**
 * Class Animal
 * @package app\modifiers
 * @author Funcraft
 */
class Animal extends LocationModifier
{
    const ATTRIBUTE = 'animal';
    const RELATION_LOCATION = 'animal';

    const PROBABILITY_NEW = 20;//percentage
    const PROBABILITY_INC = 5;
    const P_NEW = 5;

    protected $name = 'animal';

    /**
     * @var \app\models\Animal
     */
    protected $item = null;

    /**
     * @return \app\models\Location
     */
    public function apply()
    {
        $location = $this->getLocation();
        $event = new \app\events\Animal();

        if($location->hasLA(static::ATTRIBUTE)){
            $this->setItem($location->getLocationAttribute(static::ATTRIBUTE)->inc());
            $event->setContext($this->getItem())->register($location);
            $location->setLocationContent($this->getName(), $this->content());
        } elseif($landscape = $location->getLocationAttribute(Landscape::ATTRIBUTE)) {
            /**
             * @var Landscape $landscape
             */
            if($landscape->hasLand()){
                $location->setLocationContent(
                    $this->getName(),
                    $this->setItem($this->create())
                         ->content()
                );
                $event->setContext($this->getItem())->register($location);
            }
        }

        return $location;
    }

    /**
     * @param \app\models\Animal $item
     * @return $this
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return \app\models\Animal
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return string
     */
    public function content()
    {
        $item = $this->getItem();

        return $this->render([
            'alive' => $item->alive,
            'dead' => $item->dead
        ]);
    }

    /**
     * @return \app\models\Animal
     * @throws \yii\base\InvalidConfigException
     */
    public function create()
    {
        $class = \Yii::$container->get(\app\models\Animal::className());
        $animal = new $class();
        $animal->land_id = $this->getLocation()->getId();
        $animal->alive   = mt_rand(2,5);
        $animal->dead    = 0;
        $animal->created = time();
        $animal->changed = time();
        $animal->save();

        return $animal;
    }
}