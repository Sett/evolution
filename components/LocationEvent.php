<?php
namespace app\components;

use app\interfaces\IEvent;
use app\interfaces\IListener;
use app\models\Location;

/**
 * Class LocationEvent
 * @package app\components
 * @author Funcraft
 */
abstract class LocationEvent extends LocationObject implements IEvent
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var mixed
     */
    protected $context = null;

    /**
     * @var IListener[]
     */
    protected $listeners = [];

    /**
     * @var int
     */
    protected $locationId = 0;

    /**
     * @param string $name
     * @param string $type
     * @param string $parent
     * @return string
     */
    public static function constructName($name, $type, $parent = '')
    {
        return $parent . '.' . $name . '@' . $type;
    }

    /**
     * LocationEvent constructor.
     * @param mixed $context
     */
    public function __construct($context = null)
    {
        $this->setContext($context);
        parent::__construct();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $output = $this->output();
        return is_string($output) ? $output : $this->getName();
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Location $injection
     * @return $this
     */
    public function rise(&$injection)
    {
        $this->locationId = $injection->getId();

        echo '<!-- #' . $this->getName() . '#-->' . "\n";
        $listeners = $this->getListeners();

        foreach($listeners as $listener){
            if($injection->isCatchable($listener->getName())){
                $reDispatchers = $injection->getListenerReDispatchers($listener->getName());
                foreach($reDispatchers as $reDispatcher){
                    $reDispatcher->catchListener($this, $listener, $injection);
                }
            }
            $listener->dispatch($this, $injection);
        }

        return $this;
    }

    /**
     * @param \app\models\Location $location
     * @return $this
     */
    public function register(&$location)
    {
        $location->setLocationEvent($this);

        return $this;
    }

    /**
     * @param IListener $listener
     * @return $this
     */
    public function registerListener($listener)
    {
        $this->listeners[] = $listener;

        return $this;
    }

    /**
     * @return \app\interfaces\IListener[]
     */
    public function getListeners()
    {
        return $this->listeners;
    }

    /**
     * @param mixed $context
     * @return $this
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return mixed
     */
    abstract public function output();

    /**
     * @param mixed $output
     * @return $this
     */
    abstract public function setOutput($output);

    /**
     * @param mixed $output
     * @return $this
     */
    abstract public function appendOutput($output);
}