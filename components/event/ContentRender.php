<?php
namespace app\components\event;

/**
 * Class ContentRender
 * @package app\components\event
 * @author Funcraft
 */
class ContentRender extends Render
{
    /**
     * @var static
     */
    protected static $instance = null;

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     * @return $this
     */
    protected function addOutput($locationId, $eventName, $output)
    {
        $this->initLid($locationId);

        if(!isset($this->events[$locationId][$eventName])){
            $this->events[$locationId][$eventName] = '';
        }

        $this->events[$locationId][$eventName] .= $output;

        return $this;
    }
}