<?php
namespace app\components\event;

use app\components\LocationEvent;

/**
 * Class Event
 * @package app\components\event
 * @author Funcraft
 */
class Event extends LocationEvent
{
    /**
     * @param mixed $output
     * @return $this
     */
    public function setOutput($output)
    {
        EventRender::appendEventOutput($this->locationId, $this->getName(), $output);

        return $this;
    }

    /**
     * @param string $output
     * @return Event
     */
    public function appendOutput($output)
    {
        return $this->setOutput($output);
    }

    /**
     * @return string
     */
    public function output()
    {
        return EventRender::output($this->locationId);
    }
}