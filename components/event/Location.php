<?php
namespace app\components\event;

use app\components\LocationEvent;

/**
 * Class Location
 * @package app\components\event
 * @author Funcraft
 */
class Location extends LocationEvent
{
    /**
     * @param \app\models\Location $output
     * @return $this
     */
    public function setOutput($output)
    {
        LocationRender::appendEventOutput($this->locationId, $this->getName(), $output);

        return $this;
    }

    /**
     * @param \app\models\Location $output
     * @return Location
     */
    public function appendOutput($output)
    {
        return $this->setOutput($output);
    }

    /**
     * @return \app\models\Location
     */
    public function output()
    {
        return LocationRender::output($this->locationId);
    }
}