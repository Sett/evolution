<?php
namespace app\components\event;
use app\models\Location;

/**
 * Class LocationRender
 * @package app\components\event
 * @author Funcraft
 */
class LocationRender extends Render
{
    /**
     * @var static
     */
    protected static $instance = null;

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     * @return $this
     */
    protected function addOutput($locationId, $eventName, $output)
    {
        $this->events[$locationId] = $output;

        return $this;
    }

    /**
     * @param int $locationId
     * @return Location|null
     */
    public function getOutput($locationId)
    {
        return isset($this->events[$locationId]) ? $this->events[$locationId] : null;
    }
}