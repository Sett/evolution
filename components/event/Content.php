<?php
namespace app\components\event;

use app\components\LocationEvent;

/**
 * Class Content
 * @package app\components\event
 * @author Funcraft
 */
class Content extends LocationEvent
{
    /**
     * @param mixed $output
     * @return $this
     */
    public function setOutput($output)
    {
        ContentRender::appendEventOutput($this->locationId, $this->getName(), $output);

        return $this;
    }

    /**
     * @param mixed $output
     * @return $this|Content
     */
    public function appendOutput($output)
    {
        return $this->setOutput($output);
    }

    /**
     * @return string
     */
    public function output()
    {
        return ContentRender::output($this->locationId);
    }
}