<?php
namespace app\components\event;

use app\interfaces\event\IRender;
use app\interfaces\IEvent;

/**
 * Class Render
 * @package app\components\event
 * @author Funcraft
 */
abstract class Render implements IRender
{
    /**
     * @var static
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $events = [];

    /**
     * @var string[]
     */
    protected $output = [];

    /**
     * @return Render
     */
    public static function getInstance()
    {
        return static::$instance ?: static::$instance = new static();
    }

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     */
    public static function appendEventOutput($locationId, $eventName, $output)
    {
        static::getInstance()->addOutput($locationId, $eventName, $output);
    }

    /**
     * @param int $locationId
     * @return string
     */
    public static function output($locationId)
    {
        return static::getInstance()->getOutput($locationId);
    }

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     * @return $this
     */
    abstract protected function addOutput($locationId, $eventName, $output);

    /**
     * @param int $locationId
     * @return string
     */
    public function getOutput($locationId)
    {
        $this->output[$locationId] = '';

        if(!isset($this->events[$locationId])){
            return '';
        }

        foreach($this->events[$locationId] as $output){
            $this->output[$locationId] .= $output;
        }

        return $this->output[$locationId];
    }

    /**
     * @param int $locationId
     * @return $this
     */
    protected function initLid($locationId)
    {
        if(!isset($this->events[$locationId])){
            $this->events[$locationId] = [];
        }

        return $this;
    }
}