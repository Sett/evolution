<?php
namespace app\components\event;

/**
 * Class EventRender
 * @package app\components\event
 * @author Funcraft
 */
class EventRender extends Render
{
    /**
     * @var static
     */
    protected static $instance = null;

    /**
     * @param int|string $locationId
     * @param string $eventName
     * @param mixed $output
     * @return $this
     */
    protected function addOutput($locationId, $eventName, $output)
    {
        $this->initLid($locationId);

        if(!isset($this->events[$locationId][$eventName])){
            $this->events[$locationId][$eventName] = '';
        }

        $this->events[$locationId][$eventName] .= $output;

        return $this;
    }

    /**
     * @param int $locationId
     * @return string
     */
    public function getOutput($locationId)
    {
        if(!isset($this->events[$locationId])){
            return '';
        }

        $this->output[$locationId] = '
            <div class="panel panel-warning">
                <div class="panel-heading">События</div>';

        foreach($this->events[$locationId] as $output){
            $this->output[$locationId] .= '<div class="panel-body">' . $output . '</div>';
        }

        return $this->output[$locationId] . '</div>';
    }
}