<?php
namespace app\components;

use app\interfaces\ILocationModifier;
use app\models\Location;
use yii\base\View;

/**
 * Class LocationModifier
 * @package app\components
 * @author Funcraft
 */
abstract class LocationModifier extends LocationObject implements ILocationModifier
{
    const RELATION_LOCATION = self::NO_RELATION;
    const NO_RELATION = '@';

    /**
     * @var Location
     */
    protected $location = null;

    /**
     * @param Location $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @return $this
     */
    public function appendToLocation($query)
    {
        if(static::RELATION_LOCATION != self::NO_RELATION){
            $relations = is_array(static::RELATION_LOCATION)
                            ? static::RELATION_LOCATION
                            : [static::RELATION_LOCATION];

            foreach($relations as $relation){
                $query->with($relation);
            }
        }

        return $query;
    }

    /**
     * @param array $params
     * @param string $viewPath
     * @return string
     */
    public function render($params, $viewPath = '')
    {
        $view = new View();
        $viewPath = $viewPath
                    ? '@app/views/modifiers/' . $viewPath . '.php'
                    : '@app/views/modifiers/' . $this->getName() . '.php';

        return $view->render($viewPath, $params);
    }

    /**
     * @param string $name
     * @param mixed $context
     * @return mixed
     */
    public function riseEvent($name, $context)
    {
        return $this->getLocation()
                    ->registerEvent($name, $context)
                    ->getLocationEvent($name)
                    ->rise($this->getLocation())
                    ->output();
    }
}