<?php
namespace app\components;

use app\interfaces\IListener;

/**
 * Class LocationListener
 * @package app\components
 * @author Funcraft
 */
abstract class LocationListener extends LocationModifier implements IListener
{
    /**
     * @param \app\interfaces\IEvent $event
     * @param IListener $listener
     * @param \app\models\Location $location
     */
    public function catchListener(&$event, &$listener, &$location)
    {

    }
}