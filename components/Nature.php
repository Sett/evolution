<?php
namespace app\components;

use app\components\event\ContentRender;
use app\components\event\EventRender;
use app\interfaces\IEvent;
use app\interfaces\ILocationModifier;
use app\models\Location;
use yii\base\View;
use yii\db\ActiveQuery;

/**
 * Class Nature
 * @package app\components
 * @author Funcraft
 */
class Nature
{
    const VERSION = '0.9.5';
    const RELEASE_DATE = '2016/03/30';

    const CONFIG_MODIFIERS = 'modifiers';

    protected $availableModifiers = [];

    /**
     * @var array
     */
    protected $enabledModifiers = [];

    /**
     * @var ActiveQuery
     */
    protected $locationQuery = null;

    /**
     * @var Location[]
     */
    protected $locations = [];

    /**
     * @var ILocationModifier[]
     */
    protected $modifiers = [];

    /**
     * Nature constructor
     */
    public function __construct()
    {
        $newLocation = mt_rand(7, 10);
        $choose = 5;

        if($newLocation == $choose){
            $this->createLocation();
        }

        $this->initLocationQuery()
             ->loadModifiers()
             ->loadLocations()
             ->applyModifiers(
                 $this->getLocations(),
                 $this->getModifiers()
             );
    }

    /**
     * @param Location[] $locations
     * @param ILocationModifier[] $modifiers
     */
    public function applyModifiers($locations, $modifiers)
    {
        foreach($locations as $location){
            foreach($modifiers as $modifier){
                $location = $modifier->setLocation($location)->apply();
            }
        }
    }

    /**
     * @return \app\interfaces\ILocationModifier[]
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * @param ILocationModifier[] $modifiers
     * @return $this
     */
    public function setModifiers($modifiers)
    {
        $this->modifiers = $modifiers;

        return $this;
    }

    /**
     * @return $this
     */
    protected function loadEnabledModifiers()
    {
        $this->enabledModifiers = \Yii::$app->params[static::CONFIG_MODIFIERS];

        return $this;
    }

    /**
     * @return $this
     */
    public function loadModifiers()
    {
        $this->loadEnabledModifiers();

        $modifiers = [];

        foreach($this->enabledModifiers as $modifierClass){
            $modifier = \Yii::$container->get($modifierClass);
            $modifiers[] = $modifier;
            $this->setLocationQuery($modifier->appendToLocation($this->getLocationQuery()));
        }

        return $this->setModifiers($modifiers);
    }

    /**
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function createLocation()
    {
        $locationClass = \Yii::$container->get(Location::className());

        /**
         * @var Location $location
         */
        $location = new $locationClass();
        $location->square = mt_rand(100, 1001);
        $location->created = time();
        $location->changed = $location->created;
        $location->age = 1;
        $location->level = 1;
        $location->save();

        return $this;
    }

    /**
     * @return int
     */
    public function countLocations()
    {
        return count($this->getLocations());
    }

    /**
     * @return \app\models\Location[]
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param Location[] $locations
     * @return $this
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;

        return $this;
    }

    /**
     * @return Nature
     * @throws \yii\base\InvalidConfigException
     */
    public function initLocationQuery()
    {
        /**
         * @var Location $location
         */
        $location  = \Yii::$container->get(Location::className());
        return $this->setLocationQuery($location->find());
    }

    /**
     * @return ActiveQuery
     */
    public function getLocationQuery()
    {
        return $this->locationQuery;
    }

    /**
     * @param ActiveQuery $query
     * @return $this
     */
    public function setLocationQuery($query)
    {
        $this->locationQuery = $query;

        return $this;
    }

    /**
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function loadLocations()
    {
        /**
         * @var Location[] $locations
         */
        $locations = $this->getLocationQuery()->all();

        foreach($locations as $index => $location){
            $location->setLocationAttributes($location->getRelatedRecords());
            $locations[$index] = $location;
        }

        $this->setLocations($locations);

        return $this;
    }

    /**
     * @param Location $location
     * @return mixed
     */
    public function renderLocation($location)
    {
        $view = new View();

        while($events = $location->getLocationEvents()){
            foreach($events as $directive => $event){
                if($event instanceof \app\components\event\Location){
                    $location = $event->rise($location)->output();
                } else{
                   $event->rise($location);
                }
                $location->rmEvent($event->getName());
            }
        }

        return $view->render(
            '@app/views/site/location.php',
            [
                'location' => $location,
                'contents' => implode('', $location->getLocationContents())
                              . ContentRender::output($location->getId()),
                'events'   => EventRender::output($location->getId())
            ]);
    }
}