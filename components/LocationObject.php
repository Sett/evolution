<?php
namespace app\components;

use app\interfaces\ILocationObject;
use yii\db\ActiveRecord;

/**
 * Class LocationObject
 * @package app\components
 * @author Funcraft
 */
class LocationObject extends ActiveRecord implements ILocationObject
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @param string $tableName
     * @return string
     */
    public static function fullTableName($tableName)
    {
        return \Yii::$app->params['dbPrefix'] . $tableName . \Yii::$app->params['dbSuffix'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name ?: static::class;
    }

    /**
     * @param int $probability
     * @return int
     */
    public function getProbability($probability)
    {
        return mt_rand(1, 100/$probability);
    }
}